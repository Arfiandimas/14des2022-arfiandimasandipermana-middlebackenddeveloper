1. git clone https://gitlab.com/Arfiandimas/14des2022-arfiandimasandipermana-middlebackenddeveloper.git<br>
2. cd laravel-asiaquest<br>
3. Run composer install<br>
4. Run cp .env.example .env<br>
5. configure database in file .env<br>
6. Run php artisan key:generate<br>
7. Run php artisan migrate<br>
8. Run php artisan serve<br>