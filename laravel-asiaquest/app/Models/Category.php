<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    public function child()
    {
        return $this->hasMany(Category::class, 'parrent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parrent_id')
            ->with('children');
    }

    public function parrent()
    {
        return $this->belongsTo(Category::class, 'parrent_id');
    }

    public function parrents() {
        return $this->belongsTo(Category::class, 'parrent_id')
            ->with('parrent');
    }

    public function getPathAttribute()
    {
        $path = [];
        if ($this->parrent_id) {
            $parrent = $this->parrent;
            $parrent_path = $parrent->path;
            $path = array_merge($path, $parrent_path);
        }
        $path[] = $this->name;
        return $path;
    }

    public function books()
    {
        return $this->belongsToMany(Book::class, 'book_categories');
    }
}
