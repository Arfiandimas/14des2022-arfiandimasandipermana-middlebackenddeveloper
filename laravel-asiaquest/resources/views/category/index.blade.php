@extends('layout.app')
@section('content')
   <div class="category">
    <link rel="stylesheet" href="{{ asset('css/jquery.treetable.css') }}">
    <script src=" {{ asset('js/jquery.treetable.js') }}"></script>
    <table id="category_table">
        @foreach($categories as $category)
            <tr data-tt-id="{{$category->id}}" 
                {{$category->parrent_id ? "data-tt-parent-id={$category->parrent_id}" : ""}}>
                <td>
                    @if (count((array)$category->childs) == 0)
                        <input type="radio" id="category_id" name="category_id" value="{{$category->id}}">
                    @endif
                    {{ $category->name }}
                </td>
            </tr>
        @endforeach
    </table>

    <script>
        $("#category_table").treetable({ expandable: true });
    </script>
   </div>
@stop