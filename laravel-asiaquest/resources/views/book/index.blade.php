@extends('layout.app')
@section('content')
   <div class="book">
        <a href="{{route('book.create')}}" class="btn btn-success mb-3">Create</a>
        <table class="table">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">No</th>
                <th scope="col">Judul Buku</th>
                <th scope="col">Description</th>
                <th scope="col">Kategori</th>
                <th scope="col">Keyword</th>
                <th scope="col">Harga</th>
                <th scope="col">Stok</th>
                <th scope="col">Penerbit</th>
                </tr>
            </thead>
            <tbody>
            @foreach($data as $key => $book)
                <tr>
                <th scope="row">
                    <input type="checkbox" class="form-check-input ml-1" id="{{$book->id}}">
                </th>
                <td>{{ $key+1 }}</td>
                <td>{{ $book->book_title }}</td>
                <td>{{ \Illuminate\Support\Str::limit($book->description, 100, $end='...') }}</td>
                <td>
                    @foreach ($book->categories()->get() as $k => $categories)
                        {{ $categories->name }}
                        @if(count($book->categories()->get()) != $k+1)
                        ,
                        @endif
                    @endforeach
                </td>
                <td>{{ $book->keyword }}</td>
                <td>@currency($book->price)</td>
                <td>{{ $book->stock }}</td>
                <td>{{ $book->publisher }}</td>
                <td>
                    <a href="{{ route('book.show',$book->id) }}">view</a> |
                    <a href="{{ route('book.edit',$book->id) }}">edit</a> |
                    
                    <form action="{{ route('book.destroy',$book->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" style="border:none; color:blue;">Delete</button>
                    </form>
                </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="" class="btn btn-danger">Delete</a>
            </div>
            <div class="col-md-6">
                {!! $data->links() !!}
            </div>
        </div>
   </div>
@stop