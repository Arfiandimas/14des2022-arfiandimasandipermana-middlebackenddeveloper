@extends('layout.app')
@section('content')
   <div class="book-show">
        <h2 class="text-center">{{$data->book_title}}</h2>
        <p>penerbit: {{$data->publisher}}</p>
        <p>categories: 
            @foreach ($data->categories()->get() as $k => $categories)
                {{ $categories->name }}
                @if(count($data->categories()->get()) != $k+1)
                ,
                @endif
            @endforeach
        </p>
        <p>keywords: {{$data->keywords}}</p>
        <p>harga: @currency($data->price)</p>
        <p>stok: {{$data->stock}}</p>
        <p>{{$data->description}}</p>
   </div>
@stop