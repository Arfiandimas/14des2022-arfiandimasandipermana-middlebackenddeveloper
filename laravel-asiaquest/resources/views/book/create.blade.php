@extends('layout.app')
@section('content')
   <div class="book-create">
   <form autocomplete="off" id="tambah" action="{{route('book.store')}}" method="post">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="book_title">Book Title</label>
                <input type="text" class="form-control" id="book_title" name="book_title">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" row name="description"s="3"></textarea>
            </div>
            <div class="form-group">
                <label for="category">category</label>
                    <select class="form-control" name="category_id[]" multiple="">
                        @foreach($categories as $key => $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
            </div>
            <div class="form-group">
                <label for="keyword">keyword</label>
                <input type="text" class="form-control" id="keyword" name="keyword">
            </div>
            <div class="form-group">
                <label for="price">price</label>
                <input type="number" class="form-control" id="price" name="price">
            </div>
            <div class="form-group">
                <label for="stock">stock</label>
                <input type="number" class="form-control" id="stock" name="stock">
            </div>
            <div class="form-group">
                <label for="publisher">publisher</label>
                <input type="text" class="form-control" id="publisher" name="publisher">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
   </div>
@stop